#+TITLE: Emacs spell check
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil

* Why?
  It is essential when doing writing that you have some way to check your spelling errors. Especially if your someone like me that makes them frequently.

* What to do?
  1. Specify in your config a location to a spell checking program.

  =(setq ispell-program-name "C:/SGZ_Pro/z-Apps_Drivers/Hunspell/hunspell-1.3.2-3-w32-bin/bin/hunspell.exe")=
  
  1. Restart emacs
  2. Now to launch emacs spell checking programming you do =M-x flyspell-mode=

  Now emacs will highlight the words you misspelled and with =[s= you can go backwards in misspelled words and with =]s= You can go forward in misspelled words. Then z= to give a prompt of correct words that can be used to replace the misspelled words
